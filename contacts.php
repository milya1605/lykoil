<?php
$page = "contacts";
include "libs.php";

if ($_POST['submit']){
	$result = insertVopros($_POST['name'], $_POST['email'], $_POST['text']);
	if ($result){
		$_SESSION['msg'] = "<p style='color:green;'>Ваш вопрос успешно отправлен</p>";
		header("Location: contacts.php");
		exit();
	}else{
		$_SESSION['msg'] = "<p style='color:red;'>Вопрос не удалось отправить</p>";
		header("Location: contacts.php");
		exit();
	}
}
?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<title><?=$pages['title'];?></title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="header">
		<img src="images/logo.gif" alt="">
		<div class="text">
			Всегда в движении!
		</div>
	</div>

	<div class="pages">
		<div class="content">
			<div class="head">
				<div class="title">
					ГЛАВНЫЙ БИЗНЕС <span>ЛУКОЙЛ</span>
				</div>
				<img src="images/bg-home.jpg" alt="">
			</div>
			<h1><?=$pages['header'];?></h1>
			<?=$_SESSION['msg'];?>
			<p><strong>Юридический адрес и адрес центрального офиса:</strong><br />
				Россия, 101000, Москва, Сретенский бульвар, д.11</p>

			<p><strong>Контакты для акционеров:</strong><br />
				Тел.:&nbsp;8-800-200 94 02&nbsp;<br />
				Факс: (+7 495) 627 4564&nbsp;<br />
				e-mail:&nbsp;<a href="mailto:shareholder@lukoil.com">shareholder@lukoil.com</a></p>

			<p><strong>Справочная служба&nbsp;Центрального аппарата:</strong><br />
				Тел.: (+7 495) 627 4444<br />
				Факс: (+7 495)&nbsp;625 7016&nbsp;<br />
				Телекс: 612 553 LUK SU&nbsp;</p>
			<h2>Форма обратной связи</h2>
			<div class="feedback">
				<form action="" method="post">
					<div class="form-group">
						<label for="name">Имя:</label>
						<input type="text" name="name" id="name">
					</div>
					<div class="form-group">
						<label for="email">Email:</label>
						<input type="text" name="email" id="email">
					</div>
					<div class="form-group">
						<label for="text">Сообщение</label>
						<textarea name="text" id="text" cols="30" rows="10"></textarea>
					</div>
					<input type="submit" name="submit" value="Отправить">
				</form>
			</div>

		</div>

		<div class="sidebar">
			<ul>
				<li><a href="/">ГЛАВНАЯ</a></li>
				<li><a href="/about.php">О КОМПАНИИ</a></li>
				<li><a href="/news.php">НОВОСТИ</a></li>
				<li><a href="/guest_book.php">ОТЗЫВЫ</a></li>
				<li><a class="active" href="/contacts.php">КОНТАКТЫ</a></li>
			</ul>
		</div>

		<div class="foot"></div>
	</div>

	<div class="footer">
		&copy; 2016 <br>
		ПАО "Лукойл"
	</div>
</body>
</html>