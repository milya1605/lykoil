<?php
$page = "gb";
include "libs.php";

if ($_POST['submit']){
	$result = insertGB($_POST['name'], $_POST['email'], $_POST['text']);
	if ($result){
		$_SESSION['msg'] = "<p style='color:green;'>Ваш отзыв успешно отправлен</p>";
		header("Location: guest_book.php");
		exit();
	}else{
		$_SESSION['msg'] = "<p style='color:red;'>Отзыв не удалось отправить</p>";
		header("Location: guest_book.php");
		exit();
	}
}
?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<title><?=$pages['title'];?></title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="header">
		<img src="images/logo.gif" alt="">
		<div class="text">
			Всегда в движении!
		</div>
	</div>

	<div class="pages">
		<div class="content">
			<div class="head">
				<div class="title">
					ГЛАВНЫЙ БИЗНЕС <span>ЛУКОЙЛ</span>
				</div>
				<img src="images/bg-home.jpg" alt="">
			</div>
			<h1><?=$pages['header'];?></h1>
			<?=$_SESSION['msg'];?>
			<div class="gb">
				<form action="" method="post">
					<div class="form-group">
						<label for="name">Имя:</label>
						<input type="text" name="name" id="name">
					</div>
					<div class="form-group">
						<label for="email">Email:</label>
						<input type="text" name="email" id="email">
					</div>
					<div class="form-group">
						<label for="text">Текст отзыва</label>
						<textarea name="text" id="text" cols="30" rows="10"></textarea>
					</div>
					<input type="submit" name="submit" value="Отправить">
				</form>
			</div>

			<div class="list-gb">
				<? foreach($allGB as $gb):?>
				<div class="name"><?=$gb['name'];?> <span><?=$gb['email'];?></span></div>
				<div class="text">
					<?=$gb['text'];?>
				</div>
				<? endforeach;?>
			</div>

		</div>

		<div class="sidebar">
			<ul>
				<li><a href="/">ГЛАВНАЯ</a></li>
				<li><a href="/about.php">О КОМПАНИИ</a></li>
				<li><a href="/news.php">НОВОСТИ</a></li>
				<li><a class="active" href="/guest_book.php">ОТЗЫВЫ</a></li>
				<li><a href="/contacts.php">КОНТАКТЫ</a></li>
			</ul>
		</div>

		<div class="foot"></div>
	</div>

	<div class="footer">
		&copy; 2016 <br>
		ПАО "Лукойл"
	</div>
</body>
</html>
<?
unset($_SESSION['msg']);
?>