<?php
$page = "about";
include "libs.php";
?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<title><?=$pages['title'];?></title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="header">
		<img src="images/logo.gif" alt="">
		<div class="text">
			Всегда в движении!
		</div>
	</div>

	<div class="pages">
		<div class="content">
			<div class="head">
				<div class="title">
					ГЛАВНЫЙ БИЗНЕС <span>ЛУКОЙЛ</span>
				</div>
				<img src="images/bg-home.jpg" alt="">
			</div>
			<h1><?=$pages['header'];?></h1>
			<?=$pages['text'];?>
			<div class="foto">
				<h2>Наши награды</h2>
				<? foreach($allDiploms as $diploms):?>
					<img src="images/foto/<?=$diploms['name_img'];?>" alt="">
				<? endforeach;?>
			</div>
		</div>

		<div class="sidebar">
			<ul>
				<li><a href="/">ГЛАВНАЯ</a></li>
				<li><a class="active" href="/about.php">О КОМПАНИИ</a></li>
				<li><a href="/news.php">НОВОСТИ</a></li>
				<li><a href="/guest_book.php">ОТЗЫВЫ</a></li>
				<li><a href="/contacts.php">КОНТАКТЫ</a></li>
			</ul>
		</div>

		<div class="foot"></div>
	</div>

	<div class="footer">
		&copy; 2016 <br>
		ПАО "Лукойл"
	</div>
</body>
</html>