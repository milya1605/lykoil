<?php
$page = "news";
include "libs.php";
?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<title><?=$pages['title'];?></title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="header">
		<img src="images/logo.gif" alt="">
		<div class="text">
			Всегда в движении!
		</div>
	</div>

	<div class="pages">
		<div class="content">
			<div class="head">
				<div class="title">
					ГЛАВНЫЙ БИЗНЕС <span>ЛУКОЙЛ</span>
				</div>
				<img src="images/bg-home.jpg" alt="">
			</div>
			<h1><?=$pages['header'];?></h1>
			
			<? foreach($allnews as $new):?>
			<h2><?=$new['header'];?></h2>
			<div class="text-new">
				<?=$new['text'];?>
			</div>
			<? endforeach;?>

		</div>

		<div class="sidebar">
			<ul>
				<li><a href="/">ГЛАВНАЯ</a></li>
				<li><a href="/about.php">О КОМПАНИИ</a></li>
				<li><a class="active" href="/news.php">НОВОСТИ</a></li>
				<li><a href="/guest_book.php">ОТЗЫВЫ</a></li>
				<li><a href="/contacts.php">КОНТАКТЫ</a></li>
			</ul>
		</div>

		<div class="foot"></div>
	</div>

	<div class="footer">
		&copy; 2016 <br>
		ПАО "Лукойл"
	</div>
</body>
</html>